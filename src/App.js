import React from "react";

import "./App.css";
import Icons from "./components/Icons";
import ToastNotify from "./components/ToastNotify";
import Modalname from "./components/ModalName/Modalname";
import ToolTip from "./components/ToolTipp/ToolTip";
import { Border } from "./StyledComponents";
import CountUp from "./components/CountUp/CountUp";
import IdleTimer from "./components/IdleTimer/IdleTimer";
import CreditCard from "./components/CreditCard/CreditCard";
import DatePicker from "./components/DatePicker/DatePicker";
import VPlayer from "./components/ReactPlayer/VPlayer";
import Loader from "./components/Loader/Loader";
import Charts from "./components/Charts/Charts";

function App() {
  return (
    <div className="App">
      <h2>React Libraries</h2>
      <Border>
        <Icons />
      </Border>
      <Border>
        <ToastNotify />
      </Border>
      <Border>
        <Modalname />
      </Border>
      <Border>
        <ToolTip />
      </Border>
      <Border>
        <CountUp />
      </Border>
      <Border>
        <IdleTimer />
      </Border>
      <Border>
        <CreditCard />
      </Border>
      <Border>
        <DatePicker />
      </Border>
      <Border>
        <VPlayer />
      </Border>
      <Border>
        <Loader />
      </Border>
      {/* <Border>
        <Charts />
      </Border> */}
    </div>
  );
}

export default App;
