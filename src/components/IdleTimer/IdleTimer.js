import React, { useRef, useState, useEffect } from "react";
import IdleTimer from "react-idle-timer";
import Modal from "react-modal";
import Button from "@material-ui/core/Button";
import { toast } from "react-toastify";

toast.configure();

function IdleTimercomp() {
  useEffect(() => {
    toast.success(
      "You are Logged in, You can be idle for 20s to test idleTimer",
      {
        position: toast.POSITION.TOP_CENTER,
      }
    );
  }, []);
  const idleTimerRef = useRef(null);
  const [loggedIn, setLoggedIn] = useState(true);
  const [modalOpen, setModalOpen] = useState(false);
  const sessionTimeOutRef = useRef(null);
  const handleIdle = () => {
    setModalOpen(true);
    sessionTimeOutRef.current = setTimeout(handleLogOut, 10000);
  };
  const handleLogOut = () => {
    console.log("logged out");
    setModalOpen(false);
    setLoggedIn(false);
    toast.success("You are Logged out!!", {
      position: toast.POSITION.TOP_CENTER,
    });
  };
  const handleActive = () => {
    setModalOpen(false);
    setLoggedIn(true);
    toast.success("You can continue your session!!", {
      position: toast.POSITION.TOP_CENTER,
    });
  };
  return (
    <div>
      <h2>React Ideal Timer</h2>
      {loggedIn ? (
        <h3>You are Logged in</h3>
      ) : (
        <h3>You are Logged out, Refresh page to Login</h3>
      )}

      <Modal isOpen={modalOpen} onRequestClose={() => setModalOpen(false)}>
        <h2>You've been Idle for 20seconds</h2>
        <p>You will be Logged out after 10Secs, Sorry!</p>
        <div style={{ margin: "10px", display: "inline" }}>
          <Button variant="contained" color="primary" onClick={handleLogOut}>
            Log me out
          </Button>
        </div>
        <div style={{ margin: "10px", display: "inline" }}>
          <Button variant="contained" color="primary" onClick={handleActive}>
            Keep me signed in
          </Button>
        </div>
      </Modal>
      <h3>Ideal for 20seconds, you will get alert</h3>
      <IdleTimer
        timeout={2000000}
        onIdle={handleIdle}
        ref={idleTimerRef}
      ></IdleTimer>
    </div>
  );
}

export default IdleTimercomp;
