import React from "react";

import ReactPlayer from "react-player";
import { toast } from "react-toastify";
import "./Vplayer.css";

toast.configure();

function VPlayer() {
  const handleStart = () => {
    toast.success("You've started playing video", {
      position: toast.POSITION.TOP_CENTER,
    });
  };
  const handlePause = () => {
    toast.info("You've paused video", { position: toast.POSITION.TOP_CENTER });
  };
  return (
    <>
      <h2>React Player</h2>
      <div className="player">
        <ReactPlayer
          controls
          url="https://www.youtube.com/watch?v=HXQZfuSMTfM"
          onStart={handleStart}
          onPause={handlePause}
          playbackRate={1.5}
        />
      </div>
    </>
  );
}

export default VPlayer;
