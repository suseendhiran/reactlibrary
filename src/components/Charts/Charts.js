import React from "react";
import { Line } from "react-chartjs-2";
import "./Charts.css";

function Charts() {
  const data = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May"],
    datasets: [
      {
        label: "COVID Inc-Rate(Lakhs)",
        data: [5, 10, 23, 40, 56],
        borderColor: ["#f5110a"],
        backgroundColor: ["#f77874"],
      },
      {
        label: "COVID Rec-Rate(Lakhs)",
        data: [2, 4, 6, 8, 12],
        borderColor: ["#36f539"],
        backgroundColor: ["#b3f7b0"],
      },
    ],
  };
  return (
    <div className="charts">
      <Line data={data} />
    </div>
  );
}

export default Charts;
