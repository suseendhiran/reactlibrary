import React from "react";
import { BsFillBrightnessHighFill } from "react-icons/bs";
import { MdAndroid } from "react-icons/md";
import { IconContext } from "react-icons";

function Icons() {
  return (
    <div style={{ marginTop: "20px" }}>
      <h2>React icons</h2>
      <IconContext.Provider value={{ color: "#F2BA49", size: "100px" }}>
        <BsFillBrightnessHighFill />
        <MdAndroid color="green" />
      </IconContext.Provider>
    </div>
  );
}

export default Icons;
