import React from "react";
import { toast } from "react-toastify";
import Button from "@material-ui/core/Button";
import "react-toastify/dist/ReactToastify.css";

toast.configure();

const CustomToast = ({ closeToast }) => {
  return (
    <div style={{ marginTop: "20px" }}>
      Customised one
      <button onClick={closeToast}>Close</button>
    </div>
  );
};

function ToastNotify() {
  const tostify = () => {
    toast("Toastified!!", { position: toast.POSITION.TOP_CENTER });
    toast.success("Success Toastified!! Takes some time", {
      position: toast.POSITION.TOP_LEFT,
      autoClose: 10000,
      delay: 1000,
    });
    toast.error("Error Toastified!! Please close me", {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: false,
    });
    toast.warning("Warning Toastified!!", {
      position: toast.POSITION.BOTTOM_RIGHT,
    });
    toast.info("info Toastified!!", { position: toast.POSITION.BOTTOM_LEFT });

    toast.info(<CustomToast />, {
      position: toast.POSITION.BOTTOM_CENTER,
      autoClose: false,
    });
  };
  return (
    <div>
      <h2>React Toast</h2>
      <Button onClick={tostify} variant="contained" color="primary">
        Toastify!!
      </Button>
    </div>
  );
}

export default ToastNotify;
