import React, { forwardRef } from "react";
import Button from "@material-ui/core/Button";
import Tippy from "@tippy.js/react";
import "tippy.js/dist/tippy.css";
const Colored = (props) => {
  return <span style={{ color: "yellowgreen" }}>{props.cont}</span>;
};
const CustomComp = forwardRef((props, ref) => {
  return (
    <div ref={ref}>
      <div>Forward ref</div>
    </div>
  );
});
function ToolTip() {
  return (
    <div style={{ marginTop: "20px" }}>
      <h2>React ToolTip</h2>
      <div style={{ marginBottom: "20px" }}>
        <div style={{ marginBottom: "10px" }}>
          <Tippy content="No Arrow with delay 0.4s" arrow={false} delay={400}>
            <Button variant="contained" color="primary">
              Hover to Tip
            </Button>
          </Tippy>
        </div>
        <div style={{ marginBottom: "10px" }}>
          <Tippy
            content={<span style={{ color: "yellowgreen" }}>Colored</span>}
          >
            <Button variant="contained" color="primary">
              Hover to Tip
            </Button>
          </Tippy>
        </div>
        <div>
          <Tippy
            content={<Colored cont="Colored Comp + Tooltip at right" />}
            placement="right"
          >
            <Button variant="contained" color="primary">
              Hover to Tip
            </Button>
          </Tippy>
        </div>
        <div>
          <Tippy
            content={<Colored cont="Child components with forwarding ref" />}
          >
            <CustomComp />
          </Tippy>
        </div>
      </div>
    </div>
  );
}

export default ToolTip;
