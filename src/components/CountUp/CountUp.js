import React from "react";
import Button from "@material-ui/core/Button";
import Countup, { useCountUp } from "react-countup";

function CountUp() {
  const { countUp, start, pauseResume, reset, update } = useCountUp({
    duration: 5,
    end: 20000,
    prefix: "$",
    startOnMount: false,
  });
  return (
    <div>
      <h2>Countup</h2>
      <h3>{countUp}</h3>

      <div style={{ marginRight: "15px", display: "inline" }}>
        <Button variant="contained" color="primary" onClick={start}>
          Start
        </Button>
      </div>
      <div style={{ marginRight: "15px", display: "inline" }}>
        <Button variant="contained" color="primary" onClick={reset}>
          Reset
        </Button>
      </div>
      <div style={{ marginRight: "15px", display: "inline" }}>
        <Button variant="contained" color="primary" onClick={pauseResume}>
          Pause/Resume
        </Button>
      </div>
      <div style={{ marginRight: "15px", display: "inline" }}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => update(10000)}
        >
          Update to 10000
        </Button>
      </div>
    </div>
  );
}

export default CountUp;
