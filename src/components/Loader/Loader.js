import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import { PuffLoader } from "react-spinners";
import "./Loader.css";
import { css } from "@emotion/core";

function Loader() {
  const [loading, setLoading] = useState(false);
  const loaderCss = css`
    margin-top: 20px;
    margin-bottom: 30px;
  `;
  return (
    <>
      <h1>React Spinner</h1>

      <div className="loader">
        <PuffLoader css={loaderCss} loading={loading} color="#3f51b5" />
      </div>
      <Button
        variant="contained"
        color="primary"
        onClick={() => setLoading(!loading)}
      >
        Set Load
      </Button>
    </>
  );
}

export default Loader;
