import React, { useState } from "react";
import Modal from "react-modal";
import Button from "@material-ui/core/Button";

import "./ModalName.css";

Modal.setAppElement("#root");

function Modalname() {
  const [openModal, setOpenModal] = useState(false);
  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",

      color: "orange",
    },
    overlay: {
      backgroundColor: "#D3D3D3",
    },
  };
  return (
    <div style={{ marginTop: "20px" }}>
      <h2>React Modal</h2>
      <Button
        variant="contained"
        color="primary"
        onClick={() => setOpenModal(true)}
      >
        Open Modal
      </Button>
      <Modal
        isOpen={openModal}
        onRequestClose={() => {
          console.log("close");
          setOpenModal(false);
        }}
        style={{ customStyles }}
        closeTimeoutMS={400}
      >
        <div>Title</div>
        <p>Content</p>
        <p>Click Close modal button or Overlay or Esc to close me</p>
        <Button
          variant="contained"
          color="primary"
          onClick={() => setOpenModal(false)}
        >
          Close modal
        </Button>
      </Modal>
    </div>
  );
}

export default Modalname;
