import React, { useState } from "react";
import DatePicker from "react-datepicker";

import "./date.css";
import "react-datepicker/dist/react-datepicker.css";

function DatePickerr() {
  const [date, setDate] = useState(null);
  console.log(date);
  return (
    <div>
      <h1>React date picker</h1>
      <DatePicker
        className={"react-datepicker-ignore-onclickoutside "}
        selected={date}
        onChange={(date) => setDate(date)}
        dateFormat="dd/MM/yyyy"
        minDate={new Date()}
        filterDate={(date) => date.getDay() !== 0 && date.getDay() !== 6}
        isClearable={true}
        showYearDropdown
        showTimeSelect
        scrollableMonthYearDropdown
      />
    </div>
  );
}

export default DatePickerr;
